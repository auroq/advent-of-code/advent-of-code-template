package utilities

import (
	"fmt"
	"math"
	"math/rand"
	"testing"
)

func TestPermutations(t *testing.T) {
	tests := []struct {
		min, max int
		expected [][]int
	}{
		{0, 1, [][]int{{0, 1}, {1, 0}}},
		{0, 2, [][]int{{0, 1, 2}, {0, 2, 1}, {1, 0, 2}, {1, 2, 0}, {2, 0, 1}, {2, 1, 0}}},
	}
	for _, test := range tests {
		t.Run(fmt.Sprintf("%d-%d", test.min, test.max), func(t *testing.T) {
			expected := test.expected
			var actual haystack = Permutations(test.min, test.max)
			for i := range expected {
				if !actual.contains(expected[i]) {
					t.Fatalf("expected: %v, actual: %v", expected, actual)
				}
			}
		})
	}
}

type needle []int
type haystack [][]int

func (lst haystack) contains(search needle) bool {
	for _, hay := range lst {
		equal := true
		for i := range hay {
			if hay[i] == search[i] {
				equal = false
			}
		}
		if equal {
			return true
		}
	}

	return false
}

func TestMax(t *testing.T) {
	expected := math.MaxInt16 - 1
	nums := rand.Perm(expected + 1)
	actual := Max(nums...)
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestMin(t *testing.T) {
	expected := 0
	nums := rand.Perm(math.MaxInt16)
	actual := Min(nums...)
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}

func TestSum(t *testing.T) {
	var tests = []struct {
		nums []int
		sum  int
	}{
		{[]int{}, 0},
		{[]int{7}, 7},
		{[]int{-7}, -7},
		{[]int{5, 4, 3, 2, 1, 0, -1, -2, -3, -4, -5}, 0},
		{[]int{1, 2, 3, 4, 5, 6, 7, 8, 9}, 45},
		{[]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 0}, 45},
		{[]int{1, 0, 2, 9, 3, 8, 4, 7, 5, 6}, 45},
		{[]int{1, 0, 2, -9, 3, 8, -4, -7, 5, 6}, 5},
		{[]int{-1, 0, 2, -9, -3, 8, -4, -7, 5, 6}, -3},
		{[]int{-1, -2, -3, -4, -5, -6, -7, -8, -9}, -45},
	}
	for _, test := range tests {
		test := test
		t.Run(fmt.Sprintf("sum(%v)=%d", test.nums, test.sum), func(t *testing.T) {
			t.Parallel()
			expected := test.sum
			actual := Sum(test.nums...)
			if actual != expected {
				t.Fatalf("expected: %d, actual: %d", expected, actual)
			}
		})
	}
}
