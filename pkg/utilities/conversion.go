package utilities

import "strconv"

func AsToIs(as []string) (is []int, err error) {
	for _, a := range as {
		val, err := strconv.Atoi(a)
		if err != nil {
			return nil, err
		}
		is = append(is, val)
	}

	return
}
