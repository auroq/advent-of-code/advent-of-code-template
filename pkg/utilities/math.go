package utilities

import "math"

// Generate all of permutations of the set [min, max) with no duplicates
func Permutations(min, max int) (perms [][]int) {
	var nums []int
	for i := min; i <= max; i++ {
		nums = append(nums, i)
	}

	var permutations_ func(rng []int) (perms [][]int)
	permutations_ = func(rng []int) (perms [][]int) {
		if len(rng) <= 1 {
			return [][]int{rng}
		}

		for i, r := range rng {
			ps := permutations_(append(rng[i+1:], rng[:i]...)) // Always prepend not append
			for _, p := range ps {
				perms = append(perms, append([]int{r}, p...)) // Always prepend not append
			}
		}

		return
	}

	return permutations_(nums)
}

func Max(nums ...int) (max int) {
	max = math.MinInt32
	for _, val := range nums {
		if val > max {
			max = val
		}
	}

	return
}

func Min(nums ...int) (min int) {
	min = math.MaxInt32
	for _, val := range nums {
		if val < min {
			min = val
		}
	}

	return
}

func Sum(nums ...int) (sum int) {
	for _, num := range nums {
		sum += num
	}
	return
}
